# Torch Cloud Space
backing up work on git to make it workstation agnostic.


Reference URLs
- [Torch_books](https://www.learnpytorch.io/04_pytorch_custom_datasets/#exercises)
- [Interpreting Loss Curves](https://developers.google.com/machine-learning/testing-debugging/metrics/interpretic)
- [CNN_explainer](https://poloclub.github.io/cnn-explainer/#article-relu)

Features
- [ ] Data splitter in utils or data setup.
- [ ] add support for custom predict function
- [ ] add metrics parser to existing metrics
- [ ] add more metrics to the wrapper
- [ ] plotting capabilities
- [ ] find another way to impl logger (maybe decorator)

things to do
- [x] set up modular file structure
- [x] import all functions to their respective folders
- [x] document all functions
- [x] implement train.py
- [x] add arg parser support
- [x] test on something
