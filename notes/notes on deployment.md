## Deployment
- What is the most ideal mahcine learning model deployment scenario?
    - works every time
    - works very fast
- where is the model being deployed to?
    - edge devices
    - cloud
- how is the model going to function
    - online (real time)
    - offline (batch inferencing a.k.a Jobs)


### Edge devices
- very fast
- privacy perserving
- no internet connection
- fault tolerant to network

### Cloud
- unlimited compute power 
- deploy one model to use everywhere
- links to existing ecosystem.

## Tools for deployment
- edge
    - Google ML Kit
    - Apple CoreML
- general purpose
    - FastAPI
    - ONNX
    - Torch Serve
- cloud
    - google cloud vertex AI
    - AWS sage maker
    - Hugging Face with Gradio
    - azure machine learning
